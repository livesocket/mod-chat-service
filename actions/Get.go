package actions

import (
	"errors"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/mod-chat-service/models"
	"gitlab.com/livesocket/service/lib"
)

// GetAction Get a list of modchat messages
//
// public.modchat.get
// {channel string, offset uint, limit uint}
//
// Returns [ModChat...]
var GetAction = lib.Action{
	Proc:    "public.modchat.get",
	Handler: get,
}

type getInput struct {
	Channel string
	Offset  uint64
	Limit   uint64
}

func get(invocation *wamp.Invocation) client.InvokeResult {
	// Get input args from call
	input, err := getGetInput(invocation.ArgumentsKw)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Find requested amout of messages for channel
	messages, err := models.FindLatestMessagesForChannel(input.Channel, input.Offset, input.Limit)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Create wamp.List of messages
	list := wamp.List{}
	for _, message := range messages {
		list = append(list, message)
	}

	// Return list of messages
	return client.InvokeResult{Args: list}
}

func getGetInput(kwargs wamp.Dict) (*getInput, error) {
	if kwargs["channel"] == nil {
		return nil, errors.New("Missing channel")
	}

	if kwargs["offset"] == nil {
		return nil, errors.New("Missing offset")
	}

	if kwargs["limit"] == nil {
		return nil, errors.New("Missing limit")
	}

	return &getInput{
		Channel: kwargs["channel"].(string),
		Offset:  kwargs["offset"].(uint64),
		Limit:   kwargs["limit"].(uint64),
	}, nil
}
