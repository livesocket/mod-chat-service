package actions

import (
	"errors"

	"gitlab.com/livesocket/mod-chat-service/helpers"
	"gitlab.com/livesocket/mod-chat-service/models"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/service/lib"
)

// public.modchat.send

// SendAction Sends a modchat message
//
// public.modchat.send
// {channel string, message string}
//
// Returns nothing
var SendAction = lib.Action{
	Proc:    "public.modchat.send",
	Handler: send,
}

type sendInput struct {
	Channel string
	Message string
}

func send(invocation *wamp.Invocation) client.InvokeResult {
	// Get input args from call
	input, err := getSendInput(invocation.ArgumentsKw)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Create new mod chat message
	message := models.NewModChat(input.Channel, input.Message, invocation.Details["caller_authid"].(string))

	// Save to db
	result, err := message.Create()
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Get the id for the new ModChat message
	id, err := result.LastInsertId()
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Set the ID on the message
	message.ID = uint(id)

	// Emit the new message to all listeners
	helpers.EmitMessage(message)

	// Return success
	return client.InvokeResult{}
}

func getSendInput(kwargs wamp.Dict) (*sendInput, error) {
	if kwargs["channel"] == nil {
		return nil, errors.New("Missing channel")
	}

	if kwargs["message"] == nil {
		return nil, errors.New("Missing message")
	}

	return &sendInput{
		Channel: kwargs["channel"].(string),
		Message: kwargs["message"].(string),
	}, nil
}
