package main

import (
	"log"
	"os"
	"os/signal"

	"gitlab.com/livesocket/mod-chat-service/actions"
	"gitlab.com/livesocket/mod-chat-service/migrations"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

func main() {
	close, err := service.NewStandardService([]lib.Action{
		actions.GetAction,
		actions.SendAction,
	}, []lib.Subscription{}, "__mod_chat_service", migrations.CreateModChatTable)
	defer close()
	if err != nil {
		panic(err)
	}

	// Wait for CTRL-c or client close while handling events.
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	select {
	case <-sigChan:
	case <-service.Socket.Done():
		log.Print("Router gone, exiting")
		return
	}
}
