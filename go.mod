module gitlab.com/livesocket/mod-chat-service

go 1.12

require (
	github.com/gammazero/nexus/v3 v3.0.0
	github.com/jmoiron/sqlx v1.2.0
	gitlab.com/livesocket/service v1.4.3
)
