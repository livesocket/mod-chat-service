package helpers

import (
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/mod-chat-service/models"
	"gitlab.com/livesocket/service"
)

// EmitMessage Emits "public.event.modchat.message.<channel>"
func EmitMessage(modChat *models.ModChat) error {
	return service.Socket.Publish("public.event.modchat.message."+modChat.Channel, nil, wamp.List{modChat}, nil)
}
