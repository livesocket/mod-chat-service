FROM golang:1.12-alpine as base
RUN apk --no-cache add git ca-certificates g++
WORKDIR /repos/mod-chat-service
ADD go.mod go.sum ./
RUN go mod download

FROM base as builder
WORKDIR /repos/mod-chat-service
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -o mod-chat-service

FROM scratch as release
COPY --from=builder /repos/mod-chat-service/mod-chat-service /mod-chat-service
EXPOSE 8080
ENTRYPOINT ["/mod-chat-service"]
